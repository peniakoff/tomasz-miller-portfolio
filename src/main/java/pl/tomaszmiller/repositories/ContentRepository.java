package pl.tomaszmiller.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.tomaszmiller.models.Content;

import java.util.List;
import java.util.Optional;

/**
 * Created by Peniakoff on 10.06.2017.
 */
@Repository
public interface ContentRepository extends CrudRepository<Content, Integer> {

    List<Content> findAll();
    Optional<Content> findByName(String name);
    Optional<Content> findByNameAndLang(String name, String lang);

}
