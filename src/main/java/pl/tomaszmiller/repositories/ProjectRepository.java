package pl.tomaszmiller.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.tomaszmiller.models.Project;

import java.util.List;

/**
 * Created by Peniakoff on 10.06.2017.
 */
@Repository
public interface ProjectRepository extends CrudRepository<Project, Integer> {

    List<Project> findAll();
    List<Project> findAllByLang(String lang);

}
