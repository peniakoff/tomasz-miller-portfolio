//package pl.tomaszmiller;
//
//import org.apache.catalina.connector.Connector;
//import org.apache.coyote.http11.Http11NioProtocol;
//import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
//import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.io.ClassPathResource;
//
//import java.io.IOException;
//
///**
// * Created by Peniakoff on 09.06.2017.
// */
//@Configuration
//public class SslConfig {
//
//    public EmbeddedServletContainerFactory servletContainer() throws IOException {
//        TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();
//        tomcat.addAdditionalTomcatConnectors(createSslConnector());
//        return tomcat;
//    }
//
//    private Connector createSslConnector() throws IOException {
//        Connector connector = new Connector(Http11NioProtocol.class.getName());
//        Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
//        connector.setPort(8443);
//        connector.setSecure(true);
//        connector.setScheme("https");
//        protocol.setSSLEnabled(true);
//        protocol.setKeyAlias("tomaszmillersportfolio");
//        protocol.setKeystorePass("${server.ssl.key-store-password}");
//        protocol.setKeyPass("${server.ssl.key-password}");
//        protocol.setKeystoreFile(
//                new ClassPathResource("tomcat.keystore").getFile().getAbsolutePath());
//        protocol.setSslProtocol("TLS");
//        return connector;
//    }
//
//}
