package pl.tomaszmiller.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/log")
public class LogController {

    @GetMapping(value = "/ip")
    public String ipCheck() {
        return "IP Address: ";
    }

}
