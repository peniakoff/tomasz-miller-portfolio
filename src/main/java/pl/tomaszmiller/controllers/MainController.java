package pl.tomaszmiller.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import pl.tomaszmiller.repositories.ContentRepository;
import pl.tomaszmiller.services.MailService;
import pl.tomaszmiller.repositories.ProjectRepository;
import pl.tomaszmiller.config.WebConfiguration;
import pl.tomaszmiller.models.Content;
import pl.tomaszmiller.models.Message;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Locale;

/**
 * Created by Peniakoff on 04.06.2017.
 */
@Controller
public class MainController {

    private boolean isSend = false;

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    ContentRepository contentRepository;

    @Autowired
    MailService mailService;

    @Autowired
    TemplateEngine templateEngine;

    @Autowired
    WebConfiguration webConfiguration;

    @GetMapping(value = "/")
    public String index(Model model, HttpServletRequest request, HttpServletResponse response) {

        String lang = "pl";

        LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);

        if (!localeResolver.resolveLocale(request).equals(new Locale("pl", "PL"))) {
            if (!localeResolver.resolveLocale(request).equals(new Locale("pl"))) {
                localeResolver.setLocale(request, response, new Locale("en"));
                lang = "en";
            }
        }

        for (Content c : this.contentList()) {
            model.addAttribute(c.getName(), contentRepository.findByNameAndLang(c.getName(), lang).get().getText());
        }
        model.addAttribute("projects", projectRepository.findAllByLang(lang));
        model.addAttribute("message", new Message());
        if (isSend == true) {
            model.addAttribute("isSend","mailWasSend");
            isSend = false;
        }
        return "index";
    }

    @PostMapping(value = "/")
    public String email(Message message) {
        Context context = new Context();
        context.setVariable("name", message.getName());
        context.setVariable("email", message.getEmail());
        context.setVariable("phone", message.getPhone());
        context.setVariable("message", message.getMessage());
        String bodyHtml = templateEngine.process("email", context);
        mailService.sendMail(message.getEmail(), bodyHtml);
        isSend = true;
        return "redirect:/";
    }

    @GetMapping(value = "/panel")
    public String panel() {
        return "panel";
    }

    @GetMapping(value = "/login")
    @PostMapping(value = "/login")
    public String login() {
        return "login";
    }

    private List<Content> contentList() {
        List<Content> list = contentRepository.findAll();
        return list;
    }

}
